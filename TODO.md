TODO
====

* Add support for multi-part documents (headers, footers, etc.) by following
  references.
* Support other kinds of document (SpreadsheetML/xlsx, PresentationML/pptx,
  etc)
* Allow more different kinds of interaction with document content (how?)