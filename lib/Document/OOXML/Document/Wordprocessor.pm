use utf8;
package Document::OOXML::Document::Wordprocessor;
use Moose;

extends 'Document::OOXML::Document';

# ABSTRACT: Wordprocessor file 

=method remove_spellcheck_markers

=method merge_runs

Merge the content of identical adjacent runs (C<< <w:r> >> elements).

This is done internally to make finding and replacing text easy/possible.

=method find_text_nodes(qr/text/)

Finds C<< w:t >> nodes matching a specified regular expression, splits
the run around the match, and returns the matching parts.

The regular expression should not contain capture groups, as this
will mess with the text run splitting.

=method replace_text($search, $replace)

Finds all occurrences of C<$search> in the text of the document, and
replaces them with C<$replace>.

=cut

has '+document_part' => (
    handles => [qw(
        remove_spellcheck_markers
        merge_runs
        find_text_nodes
        replace_text
    )],
);

__PACKAGE__->meta->make_immutable();

=head1 SEE ALSO

=over

=item * L<Document::OOXML>

=back