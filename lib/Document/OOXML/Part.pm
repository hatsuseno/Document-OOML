use utf8;
package Document::OOXML::Part;
use Moose::Role;
use namespace::autoclean;

# ABSTRACT: Role for OOXML document parts

=head1 SYNOPSIS

    package Document::OOXML::Part::SomePart;
    use Moose;
    with 'Document::OOXML::Part';

    # used to save the part back to a file
    sub to_string { }

=cut

requires 'to_string';

has part_name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,  
);

has document => (
    is       => 'rw',
    isa      => 'Document::OOXML::Document',
    weak_ref => 1,
);

=method is_debug

Returns true if debug mode is enabled.

Debug mode can be enabled by setting the C<OOXML_DEBUG> environment
variable.

=cut

sub is_debug {
    return $ENV{OOXML_DEBUG} ? 1 : 0
}

1;

=head1 SEE ALSO

=over

=item * L<Document::OOXML>

=back